const initCalc = (arr) => {
    const productDimensions = {
        'width': parseFloat(arr[0]),
        'height': parseFloat(arr[1].toLowerCase().replace('M', '')),
    }
    calculate(productDimensions)
};

const calculate = ({width, height}) => {
    // console.log(width, height);

    const buttonCalculate = document.querySelector('.calculate');
    buttonCalculate.addEventListener('click', e => {
        e.preventDefault();
        (document.querySelector('.result p')) ? document.querySelector('.result p').remove() : null;
        const clientWidth = parseFloat(document.querySelector('#width').value);
        const clientheight = parseFloat(document.querySelector('#height').value);

        const results = document.createElement('p')
        results.textContent = `Qtda. necessária: ${Math.round(clientWidth / width )}`

        document.querySelector('.content-calc .result').appendChild(results)
    })
}

const spans = document.querySelectorAll('.product-description span');
spans.forEach((element) => {
    element.textContent.toLowerCase() == "tamanho: " ? initCalc(element.parentElement.nextElementSibling.textContent.toLowerCase().split("x")) : null;
});